import React from 'react'
import { createStackNavigator } from 'react-navigation'
import { createStore, applyMiddleware, combineReducers } from 'redux'
import { reduxifyNavigator, createReactNavigationReduxMiddleware, createNavigationReducer } from 'react-navigation-redux-helpers'
import { Provider, connect } from 'react-redux'
import thunk from 'redux-thunk'

import rootReducer from './reducers'

import HomeScreen from './containers/screens/Home'
import PhotoScreen from './containers/screens/Photo'

const AppNavigator = createStackNavigator(
  {
    Home:  { screen: HomeScreen  },
    Photo: { screen: PhotoScreen },
  },
  {
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#222',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  }
)

const navReducer = createNavigationReducer(AppNavigator)
const appReducer = combineReducers({
  nav: navReducer,
  rootReducer
})

const middleware = createReactNavigationReduxMiddleware(
  'root',
  state => state.nav,
)

const App = reduxifyNavigator(AppNavigator, 'root')
const mapStateToProps = state => ({
  state: state.nav,
})
const AppWithNavigationState = connect(mapStateToProps)(App)

const store = createStore(
  appReducer,
  applyMiddleware(thunk, middleware),
)

const Root = () => (
  <Provider store={store}>
    <AppWithNavigationState />
  </Provider>
)

export default Root