import {
  FETCH_PHOTOS_REQUEST,
  FETCH_PHOTOS_SUCCESS,
  FETCH_PHOTOS_FAILURE
} from '../../constants/actions/photos'

import { API_URL, TOKEN } from '../../constants/conf/api'

export const loadPhotos = () => dispatch => {
  dispatch({ type: FETCH_PHOTOS_REQUEST })
  fetch(`${API_URL}photos/?client_id=${TOKEN}`)
    .then(res => res.json())
    .then(data => {
      if (data.hasOwnProperty('errors')) {
        dispatch({ type: FETCH_PHOTOS_FAILURE })
      } else {
        dispatch({ type: FETCH_PHOTOS_SUCCESS, data: data })
      }
    })
    .catch(() => dispatch({ type: FETCH_PHOTOS_FAILURE }))
}