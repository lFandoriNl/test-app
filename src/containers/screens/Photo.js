import React from 'react'
import { View } from 'react-native'

import { connect } from 'react-redux'

import FullPhoto from '../../components/common/photo/PhotoFull'

class PhotoScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  render() {
    const { photo } = this.props

    return (
      <View>
        <FullPhoto url={photo.urls.regular} />
      </View>
    )
  }
}

export default connect(
  (state, ownProps) => {
    const photo = state.rootReducer.photos.list.find(photo => photo.id === ownProps.navigation.getParam('photoId'))

    return {
      photo: photo
    }
  }
)(PhotoScreen) 