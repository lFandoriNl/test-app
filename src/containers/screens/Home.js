import React from 'react'
import { StyleSheet, View } from 'react-native'

import Photos from '../../components/block/Photos'

class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Home'
  }

  render() {
    let { navigate } = this.props.navigation
    
    return (
      <View style={styles.container}>
        <Photos navigate={navigate} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#333',
  }
})

export default HomeScreen