import React from 'react'
import { StyleSheet, View, Text, Image } from 'react-native'

const PhotosItem = ({ author, url }) => (
  <View style={styles.container}>
    <Image
      style={styles.image}
      source={{ uri: url }}
    />
    <Text style={styles.text}>{author}</Text>
  </View>
)

const ITEM_HEIGHT = 300,
      ITEM_BORDER_BOTTOM_WIDTH = 5

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: ITEM_HEIGHT,
    position: 'relative',
    borderBottomColor: '#222',
    borderBottomWidth: ITEM_BORDER_BOTTOM_WIDTH,
  },
  image: {
    alignSelf: 'stretch',
    height: ITEM_HEIGHT - ITEM_BORDER_BOTTOM_WIDTH
  },
  text: {
    position: 'absolute',
    bottom: 5,
    left: 5,
    fontSize: 20,
    color: '#fff',
    textShadowColor: 'rgba(0, 0, 0, 1)',
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10
  }
})

export default React.memo(PhotosItem)