import React from 'react'
import { StyleSheet, View, Image, Dimensions  } from 'react-native'

const PhotosFull = ({ url }) => (
  <View style={styles.container}>
    <Image
      style={styles.image}
      source={{ uri: url }}
    />
  </View>
)

const DEVICE_HEIGHT = Dimensions.get('window').height

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
  },
  image: {
    height: DEVICE_HEIGHT,
    alignSelf: 'stretch',
  }
})

export default React.memo(PhotosFull)