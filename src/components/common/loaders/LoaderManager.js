import React from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, Image, Text, ActivityIndicator } from 'react-native'

class LoaderManager extends React.PureComponent {  
  render() {
    const { 
      isLoading, 
      isFailed, 
      reloadFunc, 
      componentLoading, 
      componentFailed, 
      children 
    } = this.props
    
    let loading = null
    if (componentLoading === undefined) {
      loading = <ActivityIndicator size="large" color="#0000ff" />
    } else loading = componentLoading

    let failed = null
    if (componentFailed === undefined) {
      if (reloadFunc !== undefined)
        failed = (
          <TouchableOpacity onPress={reloadFunc}>
            <Image
              style={{ width: 35, height: 35 }}
              source={require('../../../assets/images/refresh.png')}
            />
          </TouchableOpacity>
        )
      else failed = <Text style={{ fontSize: 16 }}>Failed loading photos</Text>
    } else failed = componentFailed
    
    return (
      <React.Fragment>
        {
          !isLoading ?
            !isFailed ?
              children
              :
              failed
            : 
            loading
        }
      </React.Fragment>
    )
  }
}

LoaderManager.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  isFailed: PropTypes.bool.isRequired,
  reloadFunc: PropTypes.func,
  componentLoading: PropTypes.element,
  componentFailed: PropTypes.element
}

export default LoaderManager