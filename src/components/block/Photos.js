import React from 'react'
import { StyleSheet, View  } from 'react-native'

import { connect } from 'react-redux'

import PhotosList from './PhotosList'
import LoaderManager from '../../components/common/loaders/LoaderManager'

import { loadPhotos } from '../../actions/photos'

class Photos extends React.Component {
  componentDidMount () {
    this.props.loadPhotos()
  }

  render() {
    const { isLoading, isFailed, loadPhotos, navigate } = this.props
    
    return (
      <View style={styles.container}>
        <LoaderManager
          isLoading={isLoading}
          isFailed={isFailed}
          reloadFunc={loadPhotos}
        >
          <PhotosList navigate={navigate} />
        </LoaderManager>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
  }
})

export default connect(
  state => ({
    isLoading: state.rootReducer.photos.isLoading,
    isFailed: state.rootReducer.photos.isFailed
  }),
  {
    loadPhotos
  }
)(Photos)