import React from 'react'
import { StyleSheet, FlatList, TouchableOpacity } from 'react-native'

import { connect } from 'react-redux'

import PhotoItem from '../common/photo/PhotoItem'

class PhotosList extends React.Component {
  render() {
    const { photos, navigate } = this.props
    return (
      <FlatList
        contentContainerStyle={styles.container}
        data={photos.map(item => ({ ...item, key: item.id }))}
        renderItem={({ item }) => {
          const author = item.user.first_name + ' ' + item.user.last_name
          return (
            <TouchableOpacity 
              activeOpacity={0.9} 
              onPress={() => navigate('Photo', { photoId: item.id, title: author })}
            >
              <PhotoItem
                author={author}
                url={item.urls.small} 
              />
            </TouchableOpacity>
          )
        }}
      />
    )
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-start',
  }
})

export default connect(
  state => ({
    photos: state.rootReducer.photos.list,
  })
)(PhotosList)