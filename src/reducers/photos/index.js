import {
  FETCH_PHOTOS_REQUEST,
  FETCH_PHOTOS_SUCCESS,
  FETCH_PHOTOS_FAILURE
} from '../../constants/actions/photos'

const initialState = {
  isLoading: false,
  isFailed: false,
  list: []
}

export default (state = initialState, { type, data }) => {
  switch (type) {
    case FETCH_PHOTOS_REQUEST: 
      return { ...state, isLoading: true }

    case FETCH_PHOTOS_SUCCESS:
      return { ...state, isLoading: false, isFailed: false, list: data }

    case FETCH_PHOTOS_FAILURE: 
      return { ...state, isLoading: false, isFailed: true }

    default:
      return state
  }
}